import { CurrentWeather } from "./CurrentWeather";

export class WeatherSearchResponse {
  readonly message: string;
  readonly count: number;
  readonly list: Array<CurrentWeather>;
}
