export class GeolocationCoordinates {
  readonly latitude: number;
  readonly longitude: number;
}
