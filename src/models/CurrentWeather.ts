import { Clouds } from "./Clouds";
import { Coordinates } from "./Coordinates";
import { Main } from "./Main";
import { System } from "./System";
import { Volume } from "./Volume";
import { Weather } from "./Weather";
import { Wind } from "./Wind";

export class CurrentWeather {
  readonly id: number;
  readonly name: string;
  readonly dt: Date;
  readonly coord: Coordinates;
  readonly weather: Array<Weather>;
  readonly main: Main;
  readonly wind: Wind;
  readonly clouds: Clouds;
  readonly rain: Volume;
  readonly snow: Volume;
  readonly sys: System;
}
