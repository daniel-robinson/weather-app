import { SettingsUnits } from "./SettingsUnits";

export class Settings {
  units: SettingsUnits;

  constructor(unitName?: string) {
    this.units = unitName ? new SettingsUnits(unitName) : new SettingsUnits('metric');
  }
}
