import { GeolocationCoordinates } from "./GeolocationCoordinates";

export class GeolocationResponse {
  readonly coords: GeolocationCoordinates;
}
