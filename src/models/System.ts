export class System {
  readonly country: string;
  readonly sunrise: Date;
  readonly sunset: Date;
}
