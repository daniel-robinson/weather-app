export class SettingsUnits {
  name: string;
  temperature: string;
  windSpeed: string;

  constructor(name: string) {
    this.name = name;
    this.temperature = name == 'metric' ? '°C' : '°F';
    this.windSpeed = name == 'metric' ? 'm/s' : 'mph';
  }
}
