export class Wind {
  readonly speed: number;
  readonly deg: number;
}
