export class Main {
  readonly temp: number;
  readonly pressure: number;
  readonly humidity: number;
  readonly temp_min: number;
  readonly temp_max: number;
}
