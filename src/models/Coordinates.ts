export class Coordinates {
  readonly lon: number;
  readonly lat: number;
}
