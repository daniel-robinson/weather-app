export class Weather {
  readonly id: number;
  readonly main: string;
  readonly description: string;
}
