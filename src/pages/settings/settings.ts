import { Component } from '@angular/core';
import { Settings } from '../../models/Settings';
import { SettingsProvider } from '../../providers/settings/settings';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public settingsProvider: SettingsProvider) { }

  // Updates any changed settings via the settings page
  updateSettings(): void {
    var settings = new Settings(this.settingsProvider.settings.units.name)
    this.settingsProvider.updateSettings(settings).subscribe(() => { })
  }

}
