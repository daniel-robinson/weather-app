import { Component, ViewChild } from '@angular/core';
import { Searchbar, Keyboard } from 'ionic-angular';
import { CurrentWeather } from '../../models/CurrentWeather';
import { GeolocationResponse } from '../../models/GeolcationResponse';
import { WeatherSearchResponse } from '../../models/WeatherSearchResponse';
import { CurrentWeatherProvider } from '../../providers/current-weather/current-weather';
import { GeolocationProvider } from '../../providers/geolocation/geolocation';

@Component({
  selector: 'page-weather',
  templateUrl: 'weather.html'
})
export class WeatherPage {
  geolocationResponse: GeolocationResponse;
  currentWeather: CurrentWeather;
  weatherSearchResponse: WeatherSearchResponse;
  searchTerm = '';
  showSearchbar = false;
  @ViewChild(Searchbar) private _searchbar: Searchbar;
  geolocationError = false;

  constructor(private geolocationProvider: GeolocationProvider,
    private currentWeatherProvider: CurrentWeatherProvider,
    private keyboard: Keyboard) { }

  ionViewWillEnter(): void {
    this.getCurrentLocation();
  }

  // Gets the current location using Ionic's Geolocation (https://ionicframework.com/docs/native/geolocation/)
  // and uses this get the weather for the user's location
  getCurrentLocation(): void {
    this.geolocationError = false;
    this.geolocationProvider.getCurrentLocation().subscribe((x: GeolocationResponse) => {
      this.geolocationResponse = x;
      this.searchByCoordinates(this.geolocationResponse.coords.latitude, this.geolocationResponse.coords.longitude);
    }, () => {
      this.geolocationError = true;
    })
  }

  // Searches for the weather for a given set of coordinates
  searchByCoordinates(latitude: number, longitude: number): void {
    this.currentWeatherProvider.searchByCoordinates(latitude, longitude).subscribe((x: CurrentWeather) => {
      this.currentWeather = x;
    })
  }

  // Searches for a list of cities and their corresponding weather
  searchByCity(city: string): void {
    this.currentWeatherProvider.searchByCity(city).subscribe((x: WeatherSearchResponse) => {
      this.weatherSearchResponse = x;
      this.keyboard.close();
    })
  }

  // UI method to show or hide the searchbar and change the view accordingly
  showHideSearchbar(): void {
    this.showSearchbar = !this.showSearchbar;
    if (this.showSearchbar) {
      setTimeout(() => {
        this._searchbar.setFocus();
      }, 1);
    } else {
      this.searchTerm = '';
      this.weatherSearchResponse = null;
      this.getCurrentLocation();
    }
  }

}
