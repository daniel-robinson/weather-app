import { NgModule } from '@angular/core';
import { WeatherItemComponent } from './weather-item/weather-item';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    WeatherItemComponent
  ],
  imports: [
    IonicModule.forRoot(WeatherItemComponent)
  ],
  exports: [
    WeatherItemComponent
  ]
})
export class ComponentsModule { }
