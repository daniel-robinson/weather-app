import { Component, Input } from '@angular/core';
import { CurrentWeather } from '../../models/CurrentWeather';
import { SettingsProvider } from '../../providers/settings/settings';

@Component({
  selector: 'weather-item',
  templateUrl: 'weather-item.html'
})
export class WeatherItemComponent {
  @Input('currentWeather') currentWeather: CurrentWeather;

  constructor(public settingsProvider: SettingsProvider) { }

  // UI method to put a friendly weather icon to the left of the list
  determineIcon(weatherDescription: string): string {
    if (weatherDescription.indexOf('rain') > -1) {
      return 'rainy';
    }
    if (weatherDescription.indexOf('cloud') > -1) {
      return 'cloudy';
    }
    if (weatherDescription.indexOf('snow') > -1) {
      return 'snow';
    }
    if (weatherDescription.indexOf('clear') > -1) {
      return 'sunny';
    }
    return 'partly-sunny';
  }

}
