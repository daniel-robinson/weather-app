import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs/Observable/from';
import { GeolocationResponse } from '../../models/GeolcationResponse';

@Injectable()
export class GeolocationProvider {

  constructor(private geolocation: Geolocation) { }

  // Gets the users current location using Ionic's Geolocation (https://ionicframework.com/docs/native/geolocation/),
  // maps this to the GeolocationResponse object
  getCurrentLocation(): Observable<GeolocationResponse> {
    return from(this.geolocation.getCurrentPosition({ timeout: 30000 })).pipe((x: any) => x);
  }

}
