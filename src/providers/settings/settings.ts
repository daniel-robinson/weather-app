import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs/Observable/from';
import { Settings } from '../../models/Settings';

@Injectable()
export class SettingsProvider {
  settings: Settings;
  readonly settingsKey = 'settings';

  constructor(private storage: Storage) { }

  // Gets settings from local storage for use in the application
  getSettings(): Observable<Settings> {
    return from(this.storage.get(this.settingsKey));
  }

  // Saves settings in local storage for later use in the application
  updateSettings(settings: Settings): Observable<void> {
    this.settings = settings;
    return from(this.storage.set(this.settingsKey, this.settings));
  }

}
