import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CurrentWeather } from '../../models/CurrentWeather';
import { WeatherSearchResponse } from '../../models/WeatherSearchResponse';
import { SettingsProvider } from '../settings/settings';

@Injectable()
export class CurrentWeatherProvider {
  readonly baseUrl = 'http://api.openweathermap.org/data/2.5/';
  readonly apiKey = 'bedf128b419146b1283902ef73e6bb20';

  constructor(public http: HttpClient, private settingsProvider: SettingsProvider) { }

  // Gets the weather from the API based on a set of coordinates, maps this to the CurrentWeather object
  searchByCoordinates(latitude: number, longitude: number): Observable<CurrentWeather> {
    return this.http
      .get(`${this.baseUrl}weather?lat=${latitude}&lon=${longitude}&units=${this.settingsProvider.settings.units.name}&APPID=${this.apiKey}`)
      .pipe((x: any) => x);
  }

  // Gets the weather from the API based on a query term, maps this to the WeatherSearchResponse object
  searchByCity(city: string): Observable<WeatherSearchResponse> {
    return this.http
      .get(`${this.baseUrl}find?q=${city}&units=${this.settingsProvider.settings.units.name}&APPID=${this.apiKey}`)
      .pipe((x: any) => x);
  }

}
