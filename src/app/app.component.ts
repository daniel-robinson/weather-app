import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';
import { Settings } from '../models/Settings';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsProvider } from '../providers/settings/settings';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private settingsProvider: SettingsProvider) {
    platform.ready().then(() => {
      // Get the settings saved to the app before the app loads, if no settings are saved used default
      this.settingsProvider.getSettings().subscribe((x: Settings) => {
        this.settingsProvider.settings = x || new Settings();
        this.rootPage = TabsPage;
        statusBar.styleDefault();
        splashScreen.hide();
      })
    });
  }
}
